<?php

namespace API\CommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commerce
 *
 * @ORM\Table(name="commerce")
 * @ORM\Entity(repositoryClass="API\CommerceBundle\Entity\CommerceRepository")
 */
class Commerce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="registryName", type="string", length=255, nullable=false)
     */
    private $registryName;

    /**
     * @var string
     *
     * @ORM\Column(name="taxId", type="string", unique=true, length=255, nullable=false)
     */
    private $taxId;

    /**
     * @var string
     *
     * @ORM\Column(name="urlLogo", type="string", length=255, nullable=true)
     */
    private $urlLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('TIENDA', 'HOTEL', 'RESTAURANT')", length=255, nullable= false)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registryName
     *
     * @param string $registryName
     * @return Commerce
     */
    public function setRegistryName($registryName)
    {
        $this->registryName = $registryName;

        return $this;
    }

    /**
     * Get registryName
     *
     * @return string 
     */
    public function getRegistryName()
    {
        return $this->registryName;
    }

    /**
     * Set taxId
     *
     * @param string $taxId
     * @return Commerce
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;

        return $this;
    }

    /**
     * Get taxId
     *
     * @return string 
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * Set urlLogo
     *
     * @param string $urlLogo
     * @return Commerce
     */
    public function setUrlLogo($urlLogo)
    {
        $this->urlLogo = $urlLogo;

        return $this;
    }

    /**
     * Get urlLogo
     *
     * @return string 
     */
    public function getUrlLogo()
    {
        return $this->urlLogo;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Commerce
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}
