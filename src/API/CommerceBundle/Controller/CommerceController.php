<?php

namespace API\CommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class CommerceController
 * Responsible for processing all requests relating to commerces(TIENDA, HOTEL, RESTAURANT)
 * @package API\CommerceBundle\Controller
 */
class CommerceController extends Controller
{

    /**
     * Method in charge of processing the requests to create a new commerce.
     * Parameters must be in the body of the request.
     * It can be accessed only through the verb http POST
     * url: api/commerce/new
     * @param registryName
     * @param taxId
     * @param type
     * Param type must be TIENDA, HOTEL or RESTAURANT
     * @return JsonResponse
     */
    public function newAction()
    {
        $response = $this->get('api_commerce.response');
        try {

            $serializer = $this->get('serializer');
            $validation = $this->get('api_commerce.validation');
            //valid the necessary parameters
            if ($validation::parametersExist($_POST, array('registryName', 'taxId', 'type'))) {

                // I convert the data into something of commerce type
                $commerce = $serializer->deserialize(json_encode($_POST), 'API\CommerceBundle\Entity\Commerce', 'json');

                // insert
                $em = $this->getDoctrine()->getManager();
                $em->persist($commerce);
                $em->flush();

                $jsonResponse = $response::response(200, 'Data saved successfully');
            } else {

                $jsonResponse = $response::response(400, 'Incorrect parameters');
            }

        } catch (\Exception $e) {
            $jsonResponse = $response::response(500, null, 'Error processing data');
        }

        return $jsonResponse;

    }

    /**
     * Method in charge of processing the requests to list all the existing commerces.
     * It can be accessed only through the verb http GET.
     * url: api/commerce/list
     * @return JsonResponse
     */
    public function listAction()
    {

        $response = $this->get('api_commerce.response');
        try {
            $serializer = $this->get('serializer');
            // get the records of the database
            $commerces = $this->getDoctrine()->getRepository('APICommerceBundle:Commerce')->findAll();
            // valid if there are records
            if ($commerces) {
                $jsoncontent = $serializer->serialize($commerces, 'json');
                $jsonResponse = $response::response(200, json_decode($jsoncontent));
            } else {

                $jsonResponse = $response::response(404, null, 'Not records in database');
            }
        } catch (\Exception $e) {

            $jsonResponse = $response::response(500, null, 'Error processing data');
        }

        return $jsonResponse;

    }

    /**
     * Method in charge of processing the requests to edit an existing commerces.
     * The following parameters must be in the body of the request.
     * url: api/commerce/edit/{commerceId}
     * @param registryName
     * @param taxId
     * @param urlLogo
     * @param type
     * Param type must be TIENDA, HOTEL or RESTAURANT
     * It can be accessed only through the verb http POST
     * The following parameter must be in the url
     * @param $commerceId
     * Param commerceId must be integer
     * @return JsonResponse
     */
    public function editAction($commerceId)
    {

        $response = $this->get('api_commerce.response');
        try {
            $serializer = $this->get('serializer');
            $validation = $this->get('api_commerce.validation');
            //valid the necessary parameters
            if ($validation::parametersExist($_POST, array('registryName', 'taxId', 'urlLogo', 'type'))) {

                $em = $this->getDoctrine()->getManager();
                // get the record you want to edit
                $commerce = $em->getRepository('APICommerceBundle:Commerce')->find($commerceId);
                // if the record exists
                if ($commerce) {
                    // I convert the data into something of commerce type
                    $serializer->deserialize(json_encode($_POST), 'API\CommerceBundle\Entity\Commerce', 'json',
                        array('object_to_populate' => $commerce));
                    $em->flush();

                    $jsonResponse = $response::response(200, 'Successfully updated record');
                } else {

                    $jsonResponse = $response::response(404, null, 'No record found for id '.$commerceId);

                }

            } else {

                $jsonResponse = $response::response(400, null, 'Incorrect parameters');
            }
        } catch (\Exception $e) {

            $jsonResponse = $response::response(500, null, 'Error processing data');

        }

        return $jsonResponse;
    }
}
