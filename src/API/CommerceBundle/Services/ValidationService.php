<?php
/**
 * Created by PhpStorm.
 * User: jaorr
 * Date: 10/09/17
 * Time: 11:16 PM
 */

namespace API\CommerceBundle\Services;


class ValidationService
{
    public static function parametersExist(array $data, array $campos)
    {

        foreach ($campos as $campo) {
            // valido que el campo exista en el arreglo y si existe, que no es este vacio
            if (!isset($data[$campo])) {
                return false;
            }
        }

        return true;
    }

}