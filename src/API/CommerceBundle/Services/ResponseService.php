<?php
/**
 * Created by PhpStorm.
 * User: jaorr
 * Date: 10/09/17
 * Time: 11:47 PM
 */

namespace API\CommerceBundle\Services;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class ResponseService
{

    public static function response($status, $data, $errors = null)
    {

        $response = array(
            'status' => $status,
            'data' => $data,
            'errors' => $errors,
        );

        return new JsonResponse($response);
    }

}